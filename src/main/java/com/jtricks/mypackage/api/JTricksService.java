package com.jtricks.mypackage.api;

import org.apache.log4j.Logger;

import com.atlassian.configurable.ObjectConfiguration;
import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.jira.service.AbstractService;


public class JTricksService extends AbstractService {

	org.apache.log4j.Logger log = Logger.getLogger(JTricksService.class);
	
    @Override
    public void run() {
    	log.warn("Running the JTricks service!!");
       System.out.println("Running the JTricks service!!");
    }
    @Override
    public ObjectConfiguration getObjectConfiguration() throws 
    ObjectConfigurationException {
        return getObjectConfiguration("MYNEWSERVICE", 
        "myjtricksservice.xml", null);
    }
	
}
